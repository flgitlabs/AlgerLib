using System.Configuration;
using System.Data;
using System;
using System.Data.SqlClient;

namespace AlgerSoftwareDesigns.AlgerLib
{
    /// <summary>
    /// Found this set of classes in the article, Asynchronous DataSet, http://www.williablog.net/williablog/post/2007/12/19/Asynchronous-DataSet.aspx
    /// 
    /// The author stated, "For this example I borrowed the Datalayer from the Job Site Starter Kit and added some Asynchronous methods to it. Here is my BeginExecuteDataSet method: ".
    /// 
    /// </summary>
    public class DAL
	{
		private SqlCommand cmd = new SqlCommand();
		private string strConnectionString = "";
		private bool handleErrors = false;
		private string strLastError = "";
		
        //
        //  Constructors
        //
		public DAL()
		{
            //ConnectionStringSettings objConnectionStringSettings = ConfigurationManager.ConnectionStrings["CSCJDB"];
            //strConnectionString = objConnectionStringSettings.ConnectionString;
            //SqlConnection cnn = new SqlConnection();
            //cnn.ConnectionString = strConnectionString;
            //cmd.Connection = cnn;
            //cmd.CommandType = CommandType.StoredProcedure;
		}
		
		public DAL(string connString)
		{
			strConnectionString = connString;
			SqlConnection cnn = new SqlConnection();
			cnn.ConnectionString = strConnectionString;
			cmd.Connection = cnn;
			cmd.CommandType = CommandType.StoredProcedure;
		}
		

        //
        //  ExecuteReader
        //
		public IDataReader ExecuteReader()
		{
			IDataReader reader = null;
			try
			{
				this.Open();
				reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
			}
			catch (Exception ex)
			{
				if (handleErrors)
				{
					strLastError = ex.Message;
				}
				else
				{
					throw;
				}
			}
			return reader;
		}
		
		public IDataReader ExecuteReader(string commandtext)
		{
			IDataReader reader = null;
			try
			{
				cmd.CommandText = commandtext;
				reader = this.ExecuteReader();
			}
			catch (Exception ex)
			{
				if (handleErrors)
				{
					strLastError = ex.Message;
				}
				else
				{
					throw;
				}
			}
			return reader;
		}


        //
        //  ExecuteScalar
        //
        public object ExecuteScalar()
		{
			object obj = null;
			try
			{
				this.Open();
				obj = cmd.ExecuteScalar();
				this.Close();
			}
			catch (Exception ex)
			{
				if (handleErrors)
				{
					strLastError = ex.Message;
				}
				else
				{
					throw;
				}
			}
			return obj;
		}

		public object ExecuteScalar(string commandtext)
		{
			object obj = null;
			try
			{
				cmd.CommandText = commandtext;
				obj = this.ExecuteScalar();
			}
			catch (Exception ex)
			{
				if (handleErrors)
				{
					strLastError = ex.Message;
				}
				else
				{
					throw;
				}
			}
			return obj;
		}


        //
        //  ExecuteNonQuery
        //
        public int ExecuteNonQuery()
        {
            int i = -1;
            try
            {
                this.Open();
                i = cmd.ExecuteNonQuery();
                this.Close();
            }
            catch (Exception ex)
            {
                if (handleErrors)
                {
                    strLastError = ex.Message;
                }
                else
                {
                    throw;
                }
            }
            return i;
        }

        public int ExecuteNonQuery(string commandtext)
        {
            int i = -1;
            try
            {
                cmd.CommandText = commandtext;
                i = this.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                if (handleErrors)
                {
                    strLastError = ex.Message;
                }
                else
                {
                    throw;
                }
            }
            return i;
        }


        //
        //  ExecuteDataSet
        //
        public DataSet ExecuteDataSet()
        {
            SqlDataAdapter da = null;
            DataSet ds = null;
            try
            {
                da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                ds = new DataSet();
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                if (handleErrors)
                {
                    strLastError = ex.Message;
                }
                else
                {
                    throw;
                }
            }
            return ds;
        }

        public DataSet ExecuteDataSet(string commandtext)
        {
            DataSet ds = null;
            try
            {
                cmd.CommandText = commandtext;
                ds = this.ExecuteDataSet();
            }
            catch (Exception ex)
            {
                if (handleErrors)
                {
                    strLastError = ex.Message;
                }
                else
                {
                    throw;
                }
            }
            return ds;
        }
		

		#region "Async Methods"
		
		/// <summary>
		/// There is no BeginExecuteDataset in system.Data, but we can code around this by using a datareader to get the data asynchronously then
		/// fill a dataset to return
		/// </summary>
		/// <param name="callback"></param>
		/// <param name="stateObject"></param>
		/// <param name="behavior"></param>
		/// <returns></returns>
		/// <remarks></remarks>
		public System.IAsyncResult BeginExecuteDataSet(System.AsyncCallback callback, object stateObject, CommandBehavior behavior)
		{
			IAsyncResult res = null;
			try
			{
				this.Open();
				res = cmd.BeginExecuteReader(callback, stateObject, behavior);
			}
			catch (Exception ex)
			{
				if (handleErrors)
				{
					strLastError = ex.Message;
				}
				else
				{
					throw;
				}
			}
			return res;
		}
		
		public System.IAsyncResult BeginExecuteDataSet()
		{
			return this.BeginExecuteReader(null, null, CommandBehavior.Default);
		}
		
		public System.IAsyncResult BeginExecuteDataSet(string commandtext)
		{
			cmd.CommandText = commandtext;
			return this.BeginExecuteReader(null, null, CommandBehavior.CloseConnection);
		}
		
		public System.IAsyncResult BeginExecuteDataSet(System.AsyncCallback callback, object stateObject)
		{
			return this.BeginExecuteReader(callback, stateObject, CommandBehavior.CloseConnection);
		}
		
		public IAsyncResult BeginExecuteDataSet(CommandBehavior behavior)
		{
			return this.BeginExecuteReader(null, null, behavior);
		}
		
		public DataSet EndExecuteDataSet(IAsyncResult asyncResult)
		{
			DataSet ds = null;
			try
			{
				System.Data.SqlClient.SqlDataReader rdr = cmd.EndExecuteReader(asyncResult);
				DataTable dt = new DataTable();
				dt.Load(rdr);
				ds = new DataSet();
				ds.Tables.Add(dt);
			}
			catch (Exception ex)
			{
				if (handleErrors)
				{
					strLastError = ex.Message;
				}
				else
				{
					throw;
				}
			}
			return ds;
		}
		
		
		public System.IAsyncResult BeginExecuteReader(System.AsyncCallback callback, object stateObject, CommandBehavior behavior)
		{
			IAsyncResult res = null;
			try
			{
				this.Open();
				res = cmd.BeginExecuteReader(callback, stateObject, behavior);
			}
			catch (Exception ex)
			{
				if (handleErrors)
				{
					strLastError = ex.Message;
				}
				else
				{
					throw;
				}
			}
			return res;
		}
		
		public System.IAsyncResult BeginExecuteReader()
		{
			return this.BeginExecuteReader(null, null, CommandBehavior.Default);
		}
		
		public System.IAsyncResult BeginExecuteReader(string commandtext)
		{
			cmd.CommandText = commandtext;
			return this.BeginExecuteReader(null, null, CommandBehavior.CloseConnection);
		}
		
		public System.IAsyncResult BeginExecuteReader(System.AsyncCallback callback, object stateObject)
		{
			return this.BeginExecuteReader(callback, stateObject, CommandBehavior.CloseConnection);
		}
		
		public IAsyncResult BeginExecuteReader(CommandBehavior behavior)
		{
			return this.BeginExecuteReader(null, null, behavior);
		}
		
		public SqlDataReader EndExecuteReader(IAsyncResult asyncResult)
		{
			SqlDataReader res = null;
			try
			{
				res = cmd.EndExecuteReader(asyncResult);
			}
			catch (Exception ex)
			{
				if (handleErrors)
				{
					strLastError = ex.Message;
				}
				else
				{
					throw;
				}
			}
			return res;
		}
		
		public System.IAsyncResult BeginExecuteNonQuery(System.AsyncCallback callback, object stateObject)
		{
			IAsyncResult res = null;
			try
			{
				this.Open();
				res = cmd.BeginExecuteNonQuery(callback, stateObject);
			}
			catch (Exception ex)
			{
				if (handleErrors)
				{
					strLastError = ex.Message;
				}
				else
				{
					throw;
				}
			}
			return res;
		}
		
		public System.IAsyncResult BeginExecuteNonQuery()
		{
			return this.BeginExecuteNonQuery(null, null);
		}
		
		public System.IAsyncResult BeginExecuteNonQuery(string commandtext)
		{
			cmd.CommandText = commandtext;
			return this.BeginExecuteNonQuery(null, null);
		}
		
		public int EndExecuteNonQuery(IAsyncResult asyncResult)
		{
			int res = 0;
			try
			{
				res = cmd.EndExecuteNonQuery(asyncResult);
			}
			catch (Exception ex)
			{
				if (handleErrors)
				{
					strLastError = ex.Message;
				}
				else
				{
					throw;
				}
			}
			return res;
		}
		
		#endregion
		
		//
        //  Public Properties
        //
        public string CommandText
		{
			get
			{
				return cmd.CommandText;
			}
			set
			{
				cmd.CommandText = value;
				cmd.Parameters.Clear();
			}
		}
		
		public IDataParameterCollection Parameters
		{
			get
			{
				return cmd.Parameters;
			}
		}
		
		public string ConnectionString
		{
			get
			{
				return strConnectionString;
			}
			set
			{
				strConnectionString = value;
			}
		}
		
		public bool HandleExceptions
		{
			get
			{
				return handleErrors;
			}
			set
			{
				handleErrors = value;
			}
		}
		
		public string LastError
		{
			get
			{
				return strLastError;
			}
		}
		

        //
        //  Utility Methods
        //
		public void AddParameter(string paramname, object paramvalue)
		{
			SqlParameter param = new SqlParameter(paramname, paramvalue);
			cmd.Parameters.Add(param);
		}
		
		public void AddParameter(IDataParameter param)
		{
			cmd.Parameters.Add(param);
		}
		
		private void Open()
		{
			cmd.Connection.Open();
		}
		
		private void Close()
		{
			cmd.Connection.Close();
		}
		
		public void Dispose()
		{
			cmd.Dispose();
		}
	
	}
}
