﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Diagnostics;
using System.Text;

namespace AlgerSoftwareDesigns.AlgerLib
{
    public class ProcessRun
    {
        private StringBuilder sbOutput = null;      // Place for captured output from the spawned processes.
        private StringBuilder sbError = null;

        public string[] RunCMD(String Commandline, bool CaptureOutput = false, bool CaptureError = false)
        {
            return RunExecutable("cmd.exe", string.Format("/c {0}", Commandline), CaptureOutput, CaptureError);
        }

        public string[] RunExecutable(string Filename, string ArgumentStr, bool CaptureOutput = false, bool CaptureError = false)
        {
            string[] OutputStrings = {null, null};

            sbOutput = new StringBuilder();                             // Initialize the string builders to empty
            sbError = new StringBuilder();

            Process pCmd = new Process();                               // Create a new process object

            pCmd.StartInfo.FileName = Filename;                         // Setup the process startup properties
            pCmd.StartInfo.Arguments = ArgumentStr;

            pCmd.StartInfo.UseShellExecute = false;                     // Do not use the OS shell
            pCmd.StartInfo.ErrorDialog = false;                         // No error pop-up dialog
            pCmd.StartInfo.CreateNoWindow = true;                       // Do not create a visible window for the execution

            pCmd.StartInfo.RedirectStandardOutput = CaptureOutput;      // Set the output redirect option
            pCmd.StartInfo.RedirectStandardError = true;

            if (CaptureOutput)
                pCmd.OutputDataReceived += new DataReceivedEventHandler(pCmd_OutputDataReceived);   // Set the standard output event routine

            if (CaptureError)
                pCmd.ErrorDataReceived += new DataReceivedEventHandler(pCmd_ErrorDataReceived);     // Set the standard error event routine
 
            //  Start the Process and initiate the Standard Output and Error collection if selected
            //
            pCmd.Start();                                               // Start the actual process now
            if (CaptureOutput) pCmd.BeginOutputReadLine();              // Start the asynchronous capture of standard output
            if (CaptureError) pCmd.BeginErrorReadLine();                // Start the asynchronous capture of standard error

            pCmd.WaitForExit();                                         // Now, wait for the process to complete its work

            //  Work is complete.  Place the standard output and error into a string array for return to the caller
            //
            if (CaptureOutput)
            {
                OutputStrings[0] = sbOutput.ToString();                 // Place the standard output into a string
                sbOutput = null;
            }

            if (CaptureError)
            {
                OutputStrings[1] = sbError.ToString();                  // Place the standard error into a string
                sbError = null;
            }

            //  Cleanup and return to the caller
            //
            pCmd.Close();                                               // Close out the process and free resources
            pCmd.Dispose();

            return OutputStrings;                                       // Return the true output file name
        }

        //
        //  Asynchronous output capture methods
        //
        private void pCmd_OutputDataReceived(object sender, DataReceivedEventArgs outLine)
        {
            if (!string.IsNullOrEmpty(outLine.Data))
            {
                sbOutput.Append(outLine.Data);                  // Add the new text line
                sbOutput.Append(Environment.NewLine);           // Add a newline
            }
        }

        private void pCmd_ErrorDataReceived(object sender, DataReceivedEventArgs outLine)
        {
            if (!string.IsNullOrEmpty(outLine.Data))
            {
                sbError.Append(outLine.Data);                   // Add the new text line
                sbError.Append(Environment.NewLine);            // Add a newline
            }
        }

    }
}
