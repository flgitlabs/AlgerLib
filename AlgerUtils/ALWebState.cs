﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace AlgerSoftwareDesigns.AlgerLib
{
    public class WebStateUtils
    {

        public StreamWriter CreateReplaceFile(string FilePath)
        {
            if (File.Exists(FilePath)) File.Delete(FilePath);           // Remove old file if one exists
            StreamWriter sw = new StreamWriter(FilePath);               // Create a new file and streamwriter instance
            return sw;                                                  // Return this to the caller
        }

        /// <summary>
        /// Create or replace a file and open it for text output.  The name of the file has the username embedded to make it unique.
        /// </summary>
        /// <param name="FilePath"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        public StreamWriter CreateReplaceFile(string FilePath, string username)
        {
            FilePath = AppendSuffixToFile(FilePath, username);          // Make this file username specific
            if (File.Exists(FilePath)) File.Delete(FilePath);           // Remove old file if one exists
            StreamWriter sw = new StreamWriter(FilePath);               // Create a new file and streamwriter instance
            FileStream fn = ((FileStream)sw.BaseStream);
            return sw;                                                  // Return this to the caller
        }

        public string GetStreamFileName(StreamWriter swriter)
        {
            if (!(swriter.BaseStream is FileStream))
                throw new ApplicationException("Stream object is not file based");
            return ((FileStream)swriter.BaseStream).Name;               
        }

        public string AppendSuffixToFile(string FilePath, string Suffix)
        {
            string fnType = FilePath.Substring(FilePath.LastIndexOf('.'));  // Extract the file type
            string fnName = FilePath.Substring(0, FilePath.LastIndexOf('.'));
            return string.Format("{0}_{1}{2}", fnName, Suffix, fnType);
        }

        public WebStateUtils() { }
    }
}