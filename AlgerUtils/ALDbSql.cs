﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;

namespace AlgerSoftwareDesigns.AlgerLib
{
    public class ALSqlDB
    {
        // private string _dbConnStr;
        public SqlConnection DbConn = null;
        public SqlTransaction DbTransaction = null;
        public ALCredential Credentials = new ALCredential(ALCredentialType.ADPassThrough);     // Default is current user login

        //  Methods

        /// <summary>
        /// Open a connection to the database using the specified target database.
        /// </summary>
        /// <param name="fullInstanceName">Full Instance name, 'host\instancename', of the target database</param>
        /// <param name="dbName">Database name within the instance</param>
        /// <param name="cred">Credentials to use to open the database.</param>
        /// <param name="timeout">Optional timeout in seconds</param>
        public void Open(string fullInstanceName, string dbName, ALCredential cred = null, int timeout = -1)
        {
            if (cred == null) cred = this.Credentials;                               // Default to the object credentials property
            Open(BuildSqlConnectionString(fullInstanceName, dbName, cred), timeout); // Build connection string then open the DB
        }

        /// <summary>
        /// Open a connection to the database with the given connection string
        /// </summary>
        /// <param name="ConnectionString">Fully constructed connection string</param>
        /// <param name="timeout">Optional timeout in seconds</param>
        public void Open(string ConnectionString, int timeout = -1)
        {
            //if (DbConn != null) throw new ApplicationException("Database connection already open");
            if ((DbConn != null) && (DbConn.State == ConnectionState.Open)) return;
  
            DbConn = new SqlConnection(ConnectionString);           // Create the new sql connection object
            Open(timeout);                                          // Open the database
        }

        /// <summary>
        /// Open a DB connection with the existing connection object
        /// </summary>
        /// <param name="timeout">Optional timeout in seconds</param>
        public void Open(int timeout = -1)
        {
            if (DbConn.State != ConnectionState.Open)
            {
                if (timeout >= 0)
                    DbConn.QuickOpen(timeout * 1000);   // Open under user specified timeout
                else
                    DbConn.Open();                      // Open the connection normally
            }
        }

        /// <summary>
        /// Close the database connection
        /// </summary>
        public void Close()
        {
            if (DbConn != null)                         // Check for existing connection
            {
                if (DbConn.State != ConnectionState.Closed)
                    DbConn.Close();                         // Close the database
                DbConn = null;
            }
        }


        ////  Transaction Control
        ////
        ///// <summary>
        ///// Initiate a transaction session with the open database connection
        ///// </summary>
        ///// <returns>An active SqlTransaction object</returns>
        //public SqlTransaction TransactionStart()
        //{
        //    if (this.DbTransaction != null)
        //        throw new ApplicationException("Cannot start a new transaction - Transaction already in progress");
        //    this.DbTransaction = DbConn.BeginTransaction();           // Start the transaction
        //    return this.DbTransaction;
        //}

        ///// <summary>
        ///// Commit changes made in the open transaction
        ///// </summary>
        ///// <param name="transaction">Active tranaction object</param>
        //public void TransactionCommit()
        //{
        //    if (this.DbTransaction != null)
        //    {
        //        if (DbTransaction.Connection != null)
        //            this.DbTransaction.Commit();                       // Commit all changes in this transaction
        //        this.DbTransaction = null;
        //    }
        //}

        ///// <summary>
        ///// Rollback all changes made during this tranaction
        ///// </summary>
        ///// <param name="transaction"></param>
        //public void TransactionRollback()
        //{
        //    if (this.DbTransaction != null)
        //    {
        //        if (DbTransaction.Connection != null)
        //            this.DbTransaction.Rollback();                       // Rollback all changes in this transaction
        //        this.DbTransaction = null;
        //    }
        //}


        // ====================================================================================
        //
        //  Command Submission
        //

        public List<DataTable> SubmitSqlTableCollQuery(string queryString, int timeout = -1)
        {
            return SubmitSqlTableCollQuery(CommandType.Text, queryString, timeout);
        }

        // Submit the given database command and place the result sets in a DataTableCollection object
        //
        public List<DataTable> SubmitSqlTableCollQuery(CommandType cmdType, string queryString, int timeout = -1)
        {
            SqlCommand cmdDb = new SqlCommand(queryString, DbConn);

            cmdDb.CommandType = cmdType; // Set the command type
            if (timeout >= 0)
                cmdDb.CommandTimeout = timeout; // Use supplied timeout if non-negative
            cmdDb.CommandTimeout = 15*60; // Command timeout in sec
            cmdDb.Transaction = DbTransaction;
            SqlDataAdapter daDB = new SqlDataAdapter(cmdDb); // Setup the data adapter to handle the query

            DataSet dsDB = new DataSet();
            daDB.Fill(dsDB); // Submit the query and return result in the dataset

            List<DataTable> dtc = new List<DataTable>();
            while (dsDB.Tables.Count > 0)
            {
                DataTable dt = dsDB.Tables[0];
                dsDB.Tables.Remove(dt);     // Transfer this datatable from the DataSet to the new collection
                dtc.Add(dt);
            }

            dsDB.Clear();

            cmdDb.Connection = null;
            cmdDb.Dispose();
            return dtc; // Return the tale collection
        }


        public ALSqlDBResult SubmitSqlResultQuery(CommandType cmdType, string queryString, int timeout = -1)
        {
            ALSqlDBResult qResult = new ALSqlDBResult();
            try
            {
                List<DataTable> dtColl = SubmitSqlTableCollQuery(cmdType, queryString, timeout);
                if (dtColl != null)                                           // Save the reference to the table collection
                    foreach (DataTable dt in dtColl)
                        qResult.ReturnedTables.Add(dt);                     // Move all datatables to the result list
            }
            catch (SqlException sqlEx)
            {
                qResult.CaughtException = sqlEx;                            // Save the generated exception object
            }
            return qResult;                                                 // Return the collection
        }


        /// <summary>
        /// Return the first resultset from the submitted query in a DataTable
        /// </summary>
        /// <param name="queryString"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public DataTable SubmitSqlTableQuery(string queryString, int timeout = -1)
        {
            ALSqlDBResult qResult = SubmitSqlResultQuery(CommandType.Text, queryString, timeout);
            return (qResult.ReturnedTables.Count > 0) ? qResult.ReturnedTables[0] : null;
        }

        public DataTable SubmitSqlTableQuery(CommandType cmdType, string queryString, int timeout = -1)
        {
            ALSqlDBResult qResult = SubmitSqlResultQuery(cmdType, queryString, timeout);
            return (qResult.ReturnedTables.Count > 0) ? qResult.ReturnedTables[0] : null;
        }


        /// <summary>
        /// Submit the given database command with the supplied SqlCommand oject.  No return value is expected
        /// </summary>
        /// <param name="sqlCmd">SqlCommand object</param>
        /// <param name="sqlStatement">SQL command string</param>
        public void ExecuteNonQuery(string sqlStatement, SqlCommand sqlCmd)
        {
            sqlCmd.CommandText = sqlStatement;
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.ExecuteNonQuery();
        }

        public void ExecuteNonQuery(string sqlStatement)
        {
            using (SqlCommand sqlCmd = new SqlCommand(sqlStatement, DbConn))
            {
                sqlCmd.Transaction = DbTransaction;
                ExecuteNonQuery(sqlStatement, sqlCmd);
            }
        }

        /// <summary>
        /// Submit the given database command with the supplied SqlCommand oject.  A single return object is expected
        /// </summary>
        /// <param name="sqlCmd">SqlCommand object</param>
        /// <param name="sqlStatement">SQL command string</param>
        /// <returns>Returned object from the DB query</returns>
        public object ExecuteScalar(string sqlStatement, SqlCommand sqlCmd)      // No parameters
        {
            sqlCmd.CommandText = sqlStatement;
            sqlCmd.CommandType = CommandType.Text;
            object retval = sqlCmd.ExecuteScalar();                                  // Execution returns one value
            return retval;
        }

        public object ExecuteScalar(string sqlStatement)      // No parameters
        {
            object retval = null;
            using (SqlCommand sqlCmd = new SqlCommand(sqlStatement, DbConn))
            {
                sqlCmd.Transaction = DbTransaction;
                retval = ExecuteScalar(sqlStatement, sqlCmd);
            }
            return retval;
        }


        // ================================================================================================
        //
        //  Parameter Builder
        //
        //public List<SqlParameter> NewParameterList()
        //{
        //    return new List<SqlParameter>();
        //}

        public SqlParameter BuildParam(string pName, object pValue, SqlDbType pType)   // Input parameter only - assumed
        {
            return BuildParam(pName, pValue, pType, 0, 0, ParameterDirection.Input);
        }

        public SqlParameter BuildParam(string pName, object pValue, SqlDbType pType, ParameterDirection pDirection)    // Input parameter only - assumed
        {
            return BuildParam(pName, pValue, pType, 0, 0, pDirection);
        }

        public SqlParameter BuildParam(string pName, object pValue, SqlDbType pType, int pSize)                // Input parameter only - assumed
        {
            return BuildParam(pName, pValue, pType, pSize, 0, ParameterDirection.Input);
        }

        public SqlParameter BuildParam(string pName, object pValue, SqlDbType pType, int pSize, ParameterDirection pDirection) // Input parameter only - assumed
        {
            return BuildParam(pName, pValue, pType, pSize, 0, pDirection);
        }

        public SqlParameter BuildParam(string pName, object pValue, SqlDbType pType, int pSize, int pScale)    // Input parameter only - assumed
        {
            return BuildParam(pName, pValue, pType, pSize, pScale, ParameterDirection.Input);
        }

        public SqlParameter BuildParam(string pName, object pValue, SqlDbType pType, int pSize, int pScale, ParameterDirection pDirection = ParameterDirection.Input)
        {
            SqlParameter newParam = new SqlParameter(pName, pType);
            newParam.Value = pValue;
            //newParam.Direction = (pDirection != null) ? pDirection : ParameterDirection.Input;  // Default - input parameter
            newParam.Direction = pDirection;  // Default - input parameter
            if (pType == SqlDbType.Decimal)
            {
                if (pScale > 0) newParam.Precision = (byte)pSize;
                if (pScale > 0) newParam.Scale = (byte)pScale;
            }
            else
                if (pSize > 0) newParam.Size = pSize;

            return newParam;
        }

        // ========================================================================================================
        //
        // Build the connection string
        //
        static public string BuildSqlConnectionString(string svrName, string instName, string dbName, ALCredential cred = null)
        {
            if (instName == "")
                return BuildSqlConnectionString(svrName, dbName, cred);                                 // Default Instance
            else
                return BuildSqlConnectionString(String.Concat(svrName, "\\", instName), dbName, cred);  // Named Instance
        }

        static public string BuildSqlConnectionString(string fullInstanceName, string dbName, ALCredential cred = null)
        {
            StringBuilder cnStr = new StringBuilder();

            //if (cred == null) cred = this.Credentials;                  // Use the object credentials
            if (cred == null)
                throw new ApplicationException("No credentials supplied");

            cnStr.AppendFormat("server={0};", fullInstanceName);        // Start the connection string
            if (!string.IsNullOrEmpty(dbName))
                cnStr.AppendFormat("database={0};", dbName);            // Start the connection string

            switch (cred.CType)
            {
                case ALCredentialType.SQL:                              // SQL authentication
                    cnStr.AppendFormat("user ID={0};Password={1};", cred.Username, cred.Password);
                    break;

                case ALCredentialType.ADPassThrough:                    // AD passthrough of current user
                    cnStr.Append("Trusted_Connection=True;");
                    break;

                case ALCredentialType.ADSpecified:                      // AD specified account
                    throw new ApplicationException("Cannot use a specified AD account for SQL connections");
            }
            return cnStr.ToString();
        }


        //
        //  Class Constructors
        //
        public ALSqlDB(SqlConnection connDB)
            : this()
        {
            DbConn = connDB;                                        // Save the supplied connection object
        }


        public ALSqlDB() { }
    }


    public class ALSqlDBResult
    {
        public object SingleResult = -1;
        public Exception CaughtException = null;
        public List<DataTable> ReturnedTables = new List<DataTable>();

        public ALSqlDBResult() { }
    }


    public static class SqlExtensions
    {

        /// <summary>
        /// Use a parallel thread to open a SQL connect AND timeout after a user specified amount of time.
        /// 
        /// Taken from : Mark S. Rasmussen in Development
        ///              http://improve.dk/archive/2008/03/10/controlling-sqlconnection-timeouts.aspx
        /// </summary>
        /// <param name="conn">SQL connection</param>
        /// <param name="timeout">Timeout in milliseconds</param>
        public static void QuickOpen(this SqlConnection conn, int timeout)
        {
            // We'll use a Stopwatch here for simplicity. A comparison to a stored DateTime.Now value could also be used        
            Stopwatch sw = new Stopwatch();
            bool connectSuccess = false;

            // Try to open the connection, if anything goes wrong, make sure we set connectSuccess = false        
            Thread t = new Thread(delegate()
            {
                try
                {
                    sw.Start();
                    conn.Open();
                    connectSuccess = true;
                }
                catch { }
            });

            // Make sure it's marked as a background thread so it'll get cleaned up automatically
            t.IsBackground = true;
            t.Start();

            // Keep trying to join the thread until we either succeed or the timeout value has been exceeded 
            while (timeout > sw.ElapsedMilliseconds)
                if (t.Join(1))
                    break;

            // If we didn't connect successfully, throw an exception 
            if (!connectSuccess)
                throw new ApplicationException("SqlCommand QuickOpen timed out while trying to connect.");
        }

    }

}
