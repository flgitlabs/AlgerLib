﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AlgerSoftwareDesigns.AlgerLib
{
    public static class ALCrypto
    {
        /// <summary>
        /// Generate an MD5 hash string from the supplied string
        /// </summary>
        /// <param name="input">String to be converted to an MD5 hash</param>
        /// <returns>String of the resulting MD5 hash</returns>
        public static string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input
            System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.UTF8.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }

    }
}
