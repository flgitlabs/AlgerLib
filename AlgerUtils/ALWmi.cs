﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading;


namespace AlgerSoftwareDesigns.AlgerLib
{
    public static class ALWmi
    {
        //  WMI Functions
        //

        //  Build a WMI ManagementScope object for connection to a remote server
        //
        public static ManagementScope BuildWMIConnection(string serverName, ALCredential cred)
        {
            return BuildWMIConnection(serverName, cred, @"root\cimv2");             // use default resource, \root\cimv2
        }

        public static ManagementScope BuildWMIConnection(string serverName, ALCredential cred, string wmiResource)
        {
            //  Prepare the object's values
            //        
            ConnectionOptions connOpt = new ConnectionOptions();
            if (cred.CType == ALCredentialType.ADSpecified)
            {
                connOpt.Impersonation = ImpersonationLevel.Identify;
                connOpt.Username = cred.Username;
                connOpt.Password = cred.Password;
                connOpt.Authority = string.Format("NTLMDOMAIN:{0}", cred.Domain);
            }
            else
            {
                connOpt.Authentication = AuthenticationLevel.Packet;
                connOpt.Impersonation = ImpersonationLevel.Impersonate;
            }
            return new ManagementScope(string.Format(@"\\{0}\{1}", serverName, wmiResource), connOpt);
        }

        //  Submit a query to WMI scope
        //
        public static ManagementObjectCollection QueryWMI(ManagementScope scope, string queryStmt)                   //Submit a query to WMI and wait for the results
        {
            //  Query the server physical info
            //ObjectQuery query = new ObjectQuery(queryStmt);
            //ManagementObjectSearcher searcher = new ManagementObjectSearcher(scope, query);
            //return searcher.Get();
            return TimedQueryWMI(scope, queryStmt, 60*1000);
        }


        public static ManagementObjectCollection TimedQueryWMI(ManagementScope scope, string queryStmt, int timeout)
        {
            ManagementObjectCollection collToGo = null;
            ManagementObjectSearcher searcher = null;
            ObjectQuery query = new ObjectQuery(queryStmt);
            Exception threadEx = null;

            searcher = new ManagementObjectSearcher(scope, query);
            //searcher.Options.Timeout = new TimeSpan(0, 0, 15);
            //searcher.Options.ReturnImmediately = false;

            // Try to open the connection, if anything goes wrong, capture the exception to pass back to the main thread
            //
            Thread t = new Thread(delegate()
                {
                    try
                    {
                        collToGo = searcher.Get();
                        int nRec = collToGo.Count;
                    }
                    catch (Exception ex)
                    {
                        threadEx = ex;                  // Save the exception
                    }
                });


            // We'll use a Stopwatch here for simplicity. A comparison to a stored DateTime.Now value could also be used        
            Stopwatch sw = new Stopwatch();
            sw.Start();                                 // Start the timer

            // Make sure it's marked as a background thread so it'll get cleaned up automatically
            t.IsBackground = true;
            t.Start();

            // Keep trying to join the thread until we either succeed or the timeout value has been exceeded 
            while (timeout > sw.ElapsedMilliseconds)
                if (t.Join(1))
                {
                    break;              
                }
            if (t.IsAlive)
            {
                t.Abort();
                collToGo = null;
            }


            // If we didn't connect successfully, throw an exception 
            if (threadEx != null)
                throw threadEx;                     // rethrow this error
            if (collToGo == null)
                throw new ApplicationException("WMI TimedQueryWMI timed out while trying to connect.");

            return collToGo;
        }

    }
}
