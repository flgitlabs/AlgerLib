﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AlgerSoftwareDesigns.AlgerLib
{
    /// <summary>
    /// Credential type
    /// </summary>
    public enum ALCredentialType
    {
        ADPassThrough = 1,                      // Use the AD context of the current user
        ADSpecified = 2,                        // Use the specified AD credentials
        SQL = 3                                 // SQL specific account
    }       

    /// <summary>
    /// Class to hold credential information 
    /// </summary>
    public class ALCredential
    {
        public string Username = "";            // Username
        public string Password = "";            // Password string
        public string Domain = "";              // Domain name if and active directory account
        public ALCredentialType CType;

        public string Account
        {
            get
            {
                if (CType == ALCredentialType.SQL)
                    return Username;
                else
                    return (Domain == "" ? Username : string.Concat(Domain, "\\", Username));
            }
        }

        public ALCredential(ALCredentialType cType) :this(cType, null, null, null)
        { }

        public ALCredential(ALCredentialType cType, string domain, string username, string password) : this()
        {
            switch (cType)
            {
                case ALCredentialType.ADPassThrough:
                    this.CType = cType;
                    break;

                case ALCredentialType.ADSpecified:
                    this.CType = cType;
                    this.Domain = domain;
                    this.Username = username;
                    this.Password = password;
                    break;

                case ALCredentialType.SQL:
                    this.CType = cType;
                    this.Username = username;
                    this.Password = password;
                    break;

                default:
                    throw new ApplicationException(string.Format("Unknown Credential type - {0}", cType));
            }
        }

        public ALCredential(string ctype, string username, string password)
            : this()
        {
            if (ctype.Equals("AD", StringComparison.CurrentCultureIgnoreCase))
            {
                string uname = username;
                if (uname.IndexOf('\\') < 0)                    // Check for a back slash
                    uname = string.Concat("\\", uname);
                Username = uname.Split('\\')[1];                // Parse off just the user name in domain\username
                Domain = uname.Split('\\')[0];                  // Parse off just the user name in domain\username
                if (Username.Length > 0)
                {
                    CType = ALCredentialType.ADSpecified;         // Specified username password
                    Password = password;
                }
                else
                    CType = ALCredentialType.ADPassThrough;       // Passthrough AD authentication
            }
            else if (ctype.Equals("SQL", StringComparison.CurrentCultureIgnoreCase))
            {
                CType = ALCredentialType.SQL;
                Username = username;                            // Save the username & password
                Password = password;
            }
            else
                throw new ApplicationException(string.Format("Unknown Credential type - {0}", ctype));
        }

        public ALCredential() { }
    }
}
