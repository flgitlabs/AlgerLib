﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading;

namespace AlgerSoftwareDesigns.AlgerLib
{
    //public class RegEntry
    //{
    //    public string Name;
    //    public object Value;
    //}

    public class ALRegistry
    {
        //
        //  Windows Registry Constants
        //
        private enum RegHive : uint 
        { 
            HKEY_CLASSES_ROOT = 0x80000000, 
            HKEY_CURRENT_USER = 0x80000001, 
            HKEY_LOCAL_MACHINE = 0x80000002, 
            HKEY_USERS = 0x80000003, 
            HKEY_CURRENT_CONFIG = 0x80000005 
        } 
 
        private enum RegType : int
        { 
            REG_SZ = 1, 
            REG_EXPAND_SZ, 
            REG_BINARY, 
            REG_DWORD, 
            REG_MULTI_SZ = 7 
        } 

        //
        //  Private data
        //
        private ManagementScope _mgrScope = null;
        private ConnectionOptions _copts = null;
        private ManagementPath _mgrPath = null;
        private ManagementClass _mgrClass = null;

        //
        //  Methods
        //
        //  Return the proper hive index
        //
        private uint FindReghive(string HiveName)
        {
            switch (HiveName.ToUpper())
            {
                case "CLASSES_ROOT":
                    return (uint) RegHive.HKEY_CLASSES_ROOT;

                case "CURRENT_USER":
                    return (uint) RegHive.HKEY_CURRENT_USER;

                case "LOCAL_MACHINE":
                    return (uint) RegHive.HKEY_LOCAL_MACHINE;

                case "USERS":
                    return (uint) RegHive.HKEY_USERS;

                case "CURRENT_CONFIG":
                    return (uint) RegHive.HKEY_CURRENT_CONFIG;

                default:
                    throw new ApplicationException(string.Format("Unknown registry hive name: {0}", HiveName));
            }
        }


        //  Enumerate the list of values into an array
        //
        //public void GetValueList(string HiveName, string SubkeyPath)
        //{
        //    ManagementBaseObject mboV = _mgrClass.GetMethodParameters("EnumValues");
        //    mboV["hDefKey"] = FindReghive(HiveName); 
        //    mboV["sSubKeyName"] = SubkeyPath;
        //    ManagementBaseObject outParams = _mgrClass.InvokeMethod("EnumValues", mboV, null);
            
        //}

        //  Return the a string value of the named key
        //
        public string GetStringValue(string SubkeyPath, string ValueName)
        {
            return GetStringValue("LOCAL_MACHINE", SubkeyPath, ValueName);
        }

        public string GetStringValue(string HiveName, string SubkeyPath, string ValueName)
        {
            string ReturnValue = null;
            ManagementBaseObject mboGV = _mgrClass.GetMethodParameters("GetStringValue");
            mboGV["hDefKey"] = FindReghive(HiveName);
            mboGV["sSubKeyName"] = SubkeyPath;
            mboGV["sValueName"] = ValueName;
            //ManagementBaseObject outValue = _mgrClass.InvokeMethod("GetStringValue", mboGV, null);
            ManagementBaseObject outValue = TimedGetStringValue(mboGV, 30*1000);
            if (Convert.ToUInt32(outValue["ReturnValue"]) == 0)
            {
                ReturnValue = (string)outValue["sValue"];
            }
            mboGV.Dispose();                            // Cleanup
            outValue.Dispose();
            return ReturnValue;
        }

        public ManagementBaseObject TimedGetStringValue(ManagementBaseObject mboGV, int timeout)
        {
            // We'll use a Stopwatch here for simplicity. A comparison to a stored DateTime.Now value could also be used        
            Stopwatch sw = new Stopwatch();
            bool connectSuccess = false;

            ManagementBaseObject outValue = null;

            // Try to open the connection, if anything goes wrong, make sure we set connectSuccess = false        
            Thread t = new Thread(delegate()
            {
                try
                {
                    outValue = _mgrClass.InvokeMethod("GetStringValue", mboGV, null);
                    connectSuccess = true;
                }
                catch { }
            });

            // Make sure it's marked as a background thread so it'll get cleaned up automatically
            t.IsBackground = true;
            t.Start();

            // Keep trying to join the thread until we either succeed or the timeout value has been exceeded 
            while (timeout > sw.ElapsedMilliseconds)
                if (t.Join(1))
                    break;

            // If we didn't connect successfully, throw an exception 
            if (!connectSuccess)
                throw new ApplicationException("WMI TimedGetStringValue timed out while trying to connect.");

            return outValue;
        }


        //  Return the a multi-string value of the named key
        //
        public string[] GetMultiStringValue(string SubkeyPath, string ValueName)
        {
            return GetMultiStringValue("LOCAL_MACHINE", SubkeyPath, ValueName);
        }

        public string[] GetMultiStringValue(string HiveName, string SubkeyPath, string ValueName)
        {
            string[] ReturnList = null;
            ManagementBaseObject mboGV = _mgrClass.GetMethodParameters("GetMultiStringValue");
            mboGV["hDefKey"] = FindReghive(HiveName);
            mboGV["sSubKeyName"] = SubkeyPath;
            mboGV["sValueName"] = ValueName;
            ManagementBaseObject outValue = _mgrClass.InvokeMethod("GetMultiStringValue", mboGV, null);
            if (Convert.ToUInt32(outValue["ReturnValue"]) == 0)
            {
                ReturnList = outValue["sValue"] as string[];
            }
            mboGV.Dispose();                            // Cleanup
            outValue.Dispose();
            return ReturnList;
        }

        //  Return the list of name/values in a key
        //
        public SortedDictionary<string, object> GetNameValueList(string SubkeyPath)
        {
            return GetNameValueList("LOCAL_MACHINE", SubkeyPath);
        }

         public SortedDictionary<string, object> GetNameValueList(string HiveName, string SubkeyPath)
        {
            SortedDictionary<string, object> outList = new SortedDictionary<string, object>();
            string[] sNames;
            int[] iTypes;

            //  Obtain the list of values and their types
            //
            ManagementBaseObject mboV = _mgrClass.GetMethodParameters("EnumValues");
            mboV["hDefKey"] = FindReghive(HiveName);
            mboV["sSubKeyName"] = SubkeyPath;
            ManagementBaseObject outParams = _mgrClass.InvokeMethod("EnumValues", mboV, null);

            if (Convert.ToUInt32(outParams["ReturnValue"]) != 0)
            {
                mboV.Dispose();
                outParams.Dispose();
                return null;
            }
            sNames = outParams["sNames"] as String[];
            iTypes = outParams["Types"] as int[];
 
            //  Now get the values
            //
            for (int idx = 0; idx < sNames.Count(); idx++)
            {

                string methodName = "";
                switch ((RegType)iTypes[idx])
                {
                    case RegType.REG_SZ:
                        methodName = "GetStringValue";
                        break;

                    case RegType.REG_EXPAND_SZ:
                        methodName = "GetExpandedStringValue";
                        break;

                    case RegType.REG_BINARY:
                        methodName = "GetBinaryValue";
                        break;

                    case RegType.REG_DWORD:
                        methodName = "GetDWORDValue";
                        break;

                    case RegType.REG_MULTI_SZ:
                        methodName = "GetMultiStringValue";
                        break;
                }

                ManagementBaseObject mboGV = _mgrClass.GetMethodParameters(methodName);
                mboGV["hDefKey"] = FindReghive(HiveName);
                mboGV["sSubKeyName"] = SubkeyPath;
                mboGV["sValueName"] = sNames[idx];
                ManagementBaseObject outValue = _mgrClass.InvokeMethod(methodName, mboGV, null);
                if (Convert.ToUInt32(outValue["ReturnValue"]) == 0)
                {
                    switch ((RegType)iTypes[idx])
                    {
                        case RegType.REG_SZ:
                            outList.Add(sNames[idx], (string)outValue["sValue"]);
                            break;

                        case RegType.REG_EXPAND_SZ:
                            outList.Add(sNames[idx], (string)outValue["sValue"]);
                            break;

                        case RegType.REG_BINARY:
                            outList.Add(sNames[idx], (byte[])outValue["sValue"]);
                            break;

                        case RegType.REG_DWORD:
                            outList.Add(sNames[idx], (int)outValue["sValue"]);
                            break;

                        case RegType.REG_MULTI_SZ:
                            outList.Add(sNames[idx], (string)outValue["sValue"]);
                            break;
                    }
                    mboGV.Dispose();
                    outValue.Dispose();
                }
                else
                    throw new ApplicationException(string.Format("Cannot Fetch Value \"{0}\"", sNames[idx]));
            }

            return outList;
        }

        //  Instance control
        //
        public void Open(string ServerName)
        {
            _copts = new ConnectionOptions();
            _copts.Impersonation = ImpersonationLevel.Impersonate;
            _copts.EnablePrivileges = true;

            _mgrScope = new ManagementScope(string.Format(@"\\{0}\root\default", ServerName), _copts);
            _mgrPath = new ManagementPath("StdRegProv");
            _mgrClass = new ManagementClass(_mgrScope, _mgrPath, null);
        }

        public void Close()
        {
            _mgrClass.Dispose();
        }

        public ALRegistry(string ServerName)
            : this()
        {
            this.Open(ServerName);                          // Setup this instance and open a registry reader
        }

        public ALRegistry() 
        { }

    }
}
